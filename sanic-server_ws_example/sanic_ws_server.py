from sanic import Sanic
from sanic.response import json
from sanic.websocket import WebSocketProtocol

app = Sanic()

@app.websocket('/ws')
async def feed(request, ws):
    while True:
        data = await ws.recv()
        print('Received: ' + data)

        data = 'hello!'
        print('Sending: ' + data)
        await ws.send(data)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5042, protocol=WebSocketProtocol)
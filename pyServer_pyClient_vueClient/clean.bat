cd %~dp0

@echo off

@REM "set fileList and folderList"
set filelist=
set folderlist=vue-client\node_modules vue-client\.cache vue-client\dist

@REM "remove files"
for %%a in (%filelist%) do (
	IF EXIST %%a (
		DEL %%a /F /Q
		echo [ %%a ] delete ...
	) else (
		echo [ %%a ] doesn't exist, skip ...
	)
)

@REM "folders"
for %%a in (%folderlist%) do (
	IF EXIST %%a (
		RD %%a /Q /S
		echo [ %%a ] delete ...
	) else (
		echo [ %%a ] doesn't exist, skip ...
	)
)

import asyncio
import websockets
import time

# 參數 websocket，是 client handler，
# 有 websocket-client 連線進來時，才會建立 websocket，並帶入第一個參數中
async def hello(websocket, path):
    """
    client_ip, client_port, _, _ = websocket.remote_address

    if client_ip == "::1":
        client_ip = "localhost"

    print(f"{client_ip}:{client_port} is conneted. ")
    """
    print(websocket.remote_address)

    # ==== wait and receive client message ====
    client_msg = await websocket.recv()
    print(f"< {client_msg}")

    # ==== send message to client ====
    greeting = f"Hello {client_msg}!"
    # simulate lazy response
    time.sleep(5)
    await websocket.send(greeting)
    print(f"> {greeting}")

    # !! 注意，send 結束時會自動發送 close event 給 客戶端 !!

ip = "localhost"
port = 8765

start_server = websockets.serve(hello, ip, port)
print("Websocket server is listening @ localhost:8765")

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
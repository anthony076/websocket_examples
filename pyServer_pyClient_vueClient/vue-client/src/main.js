import Vue from 'vue'
import Ws from './Ws'

new Vue({
    el: '#app',
    render: h => h(Ws)
})


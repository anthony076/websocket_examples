
# 執行 browser-websocket-client with vue 的方式

- 技術線
    - server，python websockets module
    - client，python websockets module
    - client，javascript vue + native websocket

- 執行方法一，透過 parcel dev-server<br>
npm run dev

- 執行方法二，編譯後，手動執行<br>
    - 2-0，
        cd vue-client
        npm install

    - 2-1，parcel build index.html `--public-url .` <br>
    `--public-url` . 會讓 parcel 使用相對路徑存取資源

    - 2-2，手動在瀏覽器中開啟 `dist\index.html`

- 執行方法三，編譯後，透過 vscode 的 live-server 執行<br>
    - 3-1，parcel build index.html `--public-url .` <br>
    `--public-url` . 會讓 parcel 使用相對路徑存取資源
    - 3-2，在 dist\index.html 的檔案中，點選右鍵，`選擇 Open with Live-Server` <br>
    `選擇 Open with Live-Server`，會將 live-server 開在當前檔案的路徑上




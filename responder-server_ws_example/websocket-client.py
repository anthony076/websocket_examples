import asyncio, websockets, random

async def hello():
    async with websockets.connect('ws://localhost:5042/ws') as websocket:

        await websocket.send(str(random.randint(0,100)))
        num = await websocket.recv()
        print(f"{num}")

asyncio.get_event_loop().run_until_complete(hello())
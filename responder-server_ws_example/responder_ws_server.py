import responder, time

api = responder.API()

@api.route('/ws', websocket=True)
async def websocket(ws):
    # 處理連線
    await ws.accept()
    print(f"client_state: {ws.client_state}")

    # 接收訊息
    num = await ws.receive_text()
    print(f"receive:{num}")

    # 傳送訊息
    await ws.send_text(f"Hello {num}!")

api.run()

